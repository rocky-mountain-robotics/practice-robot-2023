// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import java.text.Normalizer;

import org.opencv.core.Mat;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.drive.DifferentialDrive;

/** Add your docs here. */
public class DriveTrain {

    private CANSparkMax motorL1;
    private CANSparkMax motorL2;
    private CANSparkMax motorR1;
    private CANSparkMax motorR2;

    private DifferentialDrive drive;

    public DriveTrain() {
        motorL1 = new CANSparkMax(6, MotorType.kBrushless);
        motorL2 = new CANSparkMax(7, MotorType.kBrushless);
        motorR1 = new CANSparkMax(9, MotorType.kBrushless);
        motorR2 = new CANSparkMax(8, MotorType.kBrushless);

        motorL1.setInverted(true);
        motorL2.setInverted(true);
        motorR1.setInverted(false);
        motorR2.setInverted(false);

        motorL2.follow(motorL1);
        motorR2.follow(motorR1);

    }

    public void driveForward(double forwardSpeed) {
        motorL1.set(forwardSpeed);
        motorR1.set(forwardSpeed);
    }

    public void stopDrive() {
        motorL1.set(0);
        motorR1.set(0);
    }

    public void driveAndTurn(double forwardSpeed, double turnSpeed) {
        double leftSpeed = (forwardSpeed + turnSpeed);
        double rightSpeed = (forwardSpeed - turnSpeed);

        //takes the larger value of the absalute value of both speeds
        double norm = Math.max(Math.abs(leftSpeed), Math.abs(rightSpeed));

        if (norm > 1) {
            leftSpeed = leftSpeed / norm;
            rightSpeed = rightSpeed / norm;
        }

        motorL1.set(leftSpeed);
        motorR1.set(rightSpeed);
    }
}
